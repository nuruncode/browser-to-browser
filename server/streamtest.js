var dl = require('delivery'),
    fs = require('fs');

var static = require('node-static');
var http = require('http');
var file = new (static.Server)();

var app = http.createServer(function (req, res) {
    file.serve(req, res);
}).listen(2503);//80 or //8080

var io = require('socket.io').listen(app);

var SocketServiceEvent = require("../htdocs/js/events/SocketServiceEvent");

io.of("/image").on('connection', function (socket) {
    // convenience function to log server messages on the client
    function log() {
        var array = [">>> Message from server: "];
        for (var i = 0; i < arguments.length; i++) {
            array.push(arguments[i]);
        }
        socket.emit('log', array);
    }

    socket.on(SocketServiceEvent.CREATE_OR_JOIN, function (user) {
        socket.get('user', function (err, storedUser) {
            log("get user:", storedUser);
            if (err) {
                log("error", err);
                return;
            }
            if (storedUser) {
                log('User already joined', storedUser);
                if (user.token === storedUser.token) {
                    socket.emit('ready');
                } else {
                    socket.emit('full', user);
                }

            } else {

                // var numClients = io.sockets.clients(user.id).length;
                var numClients = io.of('/image').clients(user.token).length;
                log('User ' + user.id + ' has ' + numClients + ' client(s)');
                log('Request to create user ' + user.name);
                if (numClients < 2) {
                    socket.join(user.token);
                    socket.set('user', user, function () {
                        log("set user:", user.token);
                        socket.emit(SocketServiceEvent.READY);
                    });
                } else { // max 1 client

                    socket.emit(SocketServiceEvent.FULL, user);
                }
            }

        });


    });
    socket.on('disconnect', function () {
        socket.get("user", function (err, user) {

            if (err) {
                log("error", err);
                return;
            }
            if (!user) {
                socket.emit(SocketServiceEvent.ERROR, SocketServiceEvent.NO_USER_SONNECTED);
                return;
            }
            user.mobileConnected = false;
            io.of('/image').in(user.token).emit(SocketServiceEvent.MOBILE_DISCONNECTED);
        });
    });
    socket.on(SocketServiceEvent.MOBILE_CONNECTED, function () {

        socket.get("user", function (err, user) {
            log(SocketServiceEvent.MOBILE_CONNECTED, "user:", user.token);
            if (err) {
                log("error", err);
                return;
            }
            if (!user) {
                socket.emit(SocketServiceEvent.ERROR, SocketServiceEvent.NO_USER_SONNECTED);
                return;
            }
            user.mobileConnected = true;
            io.of('/image').in(user.token).emit(SocketServiceEvent.MOBILE_CONNECTED);
        });
    });

    socket.on(SocketServiceEvent.MESSAGE_FROM_GAME, function (data) {
        socket.get("user", function (err, user) {
            if (err) {
                log("error", err);
                return;
            }
            if (!user) {
                socket.emit(SocketServiceEvent.ERROR, SocketServiceEvent.NO_USER_SONNECTED);
                return;
            }
            log("message from game:", user.token);
            io.of('/image').in(user.token).emit(SocketServiceEvent.MESSAGE_FROM_GAME, data);
        });
        //socket.broadcast.emit('message from game', data);

    });


    var delivery = dl.listen(socket);
    delivery.on('receive.success', function (file) {
        function savefile(user, file) {
            var _now = new Date().getTime();
            var _suffix = file.name ? file.name : ".png";
            var _name = 'photo_' + _now + "_" + _suffix;
            var save_filepath = '../htdocs/img/' + _name;
            var relativePath = 'img/' + _name;
            fs.writeFile(save_filepath, file.buffer, function (err) {
                if (err) {
                    log('File could not be saved.');
                } else {
                    log('File saved.');
                    var image = {
                        name: _name,
                        path: relativePath

                    }
                    socket.emit(SocketServiceEvent.FILE_SAVED, image);
                    if (user.mobileConnected) {
                        io.of('/image').in(user.token).emit(SocketServiceEvent.FILE_SAVED_BROADCAST, image);
                    }

                    // socket.broadcast.emit('file-saved-broadcast', image);
                }
                ;
            });
        }

        socket.get("user", function (err, user) {
            if (err) {
                log("error", err);
                return;
            }
            if (!user) {
                socket.emit(SocketServiceEvent.ERROR, SocketServiceEvent.NO_USER_SONNECTED);
                return;
            }
            savefile(user, file);
        });

    });

    /*delivery.on('send.success', function (file) {
     log('File successfully sent to client!');
     });
     */
});


