library b2b;
import 'dart:html' as html;
import "packages/box2d/box2d_browser.dart";
import "packages/stagexl/stagexl.dart";
import "dart:js" as js;

RenderLoop renderLoop = new RenderLoop();

Stage stage;

World world;

double SCALE = 1 / 30;

Frisbi frisbi = new Frisbi();

void main() {
  stage = new Stage("Stage", html.querySelector("#stage"));
  renderLoop.addStage(stage);
  if (Multitouch.supportsTouchEvents) {
    Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
  } else {

  }
  var gravity = new Vector2(0.0, 0.0);
  world = new World(gravity, true, new DefaultWorldPool());

  Bitmap c = new Bitmap(new BitmapData(320, 480, false, 0xffcc00));

  stage.addChild(c);
  stage.addChild(frisbi);
  stage.onEnterFrame.listen((e) {
    world.step(1 / 60, 10, 10);
    frisbi.update();
    if (frisbi.y < -100) {
      _reset();
    }
  });
}

_reset() {
  _callJs();
  frisbi.reset();


}

_callJs() {
  var jsMap = new js.JsObject.jsify({
      "linearVelocity":{
          "x":frisbi.x, "y":frisbi.y
      }, "inertia":frisbi.body.inertia
  });

  final b2b = js.context['b2b'];
  new js.JsObject(b2b["receive"], [jsMap]);


}

class Frisbi extends Sprite {
  Body body;

  CircleShape circleShape = new CircleShape();

  final FixtureDef circleFixtureDef = new FixtureDef();

  BodyDef def = new BodyDef();

  Frisbi() {

    circleShape.radius = 24.0 * SCALE;
/*circleFixtureDef.density=1.0;*/

    circleFixtureDef.shape = circleShape;
    circleFixtureDef.friction = 0.10;
    circleFixtureDef.restitution = 1.0;


    def.linearDamping = 0.2;
    def.type = BodyType.DYNAMIC;
    def.position.x = 160.0 * SCALE;
    def.position.y = 280.0 * SCALE;
    def.allowSleep = true;

    body = world.createBody(def);
    body.fixedRotation = false;
    body.mass = 1.1;
    body.createFixture(circleFixtureDef);
    ResourceManager rm = new ResourceManager();
    rm.addBitmapData("frisbi", "photo.jpg");
    rm.load().then((e) {
      Bitmap image = new Bitmap(rm.getBitmapData("frisbi"));
      image.pivotX = 24;
      image.pivotY = 24;
      addChild(image);
      if (Multitouch.supportsTouchEvents) {
        _listenTouch();
      } else {
        _listenMouse();
      }
    });

  }

  update() {
    x = body.position.x / SCALE;
    y = body.position.y / SCALE;

    rotation = body.angle;
  }

  Vector2 origin;

  Vector2 destination;

  Vector2 first;

  var subEF;


  _handleTouchMove() {
    subEF = stage.onTouchMove.listen((TouchEvent e) {
      var _new = new Vector2(-e.stageX * SCALE, -e.stageY * SCALE);
      var appoggio = (first - _new) * 9.0;
      body.applyLinearImpulse(appoggio, first);
      first = _new;
    });
  }

  _listenTouch() {
    onTouchBegin.listen((TouchEvent e) {
      e.stopPropagation();
      try{
        subEF.cancel();
      }catch(e){
        //
      }
      var v = new Vector2(-e.stageX * SCALE, -e.stageY * SCALE);
      _handleMouseDown(v);
      _handleTouchMove();
    });
    //onTouchEnd.listen(_handleStage);
    stage.onTouchEnd.listen(_handleStage);
    //stage.onTouchCancel.listen(_handleStage);
  }

  _listenMouse() {
    onMouseDown.listen((e) {
      var v = new Vector2(-stage.mouseX * SCALE, -stage.mouseY * SCALE);
      _handleMouseDown(v);
      _handleEnterFrame();
    });

    stage.onMouseRollOut.listen(_handleStage);
    stage.onMouseUp.listen(_handleStage);
  }

  _handleEnterFrame() {
    subEF = onEnterFrame.listen((e) {
      var _new = new Vector2(-stage.mouseX * SCALE, -stage.mouseY * SCALE);

      var appoggio = (first - _new) * 3.70;

      body.applyLinearImpulse(appoggio, first);
      first = _new;
/* body.position.x=stage.mouseX;
      body.position.y=stage.mouseY;*/
    });
  }

  _handleMouseDown(Vector2 v) {
    first = origin = v;
    body.linearDamping = 5.2;

  }

  _handleStage(e) {
    body.linearDamping = 0.2;
    try {
      subEF.cancel();
    } catch(e) {
//
    }

  }

  reset() {

    body
      ..angularVelocity = 0.0
      ..linearVelocity = new Vector2.zero()
      ..setTransform(new Vector2(160.0 * SCALE, 280 * SCALE), 0.0);
    world.clearForces();

  }
}


