/**
 * Created by marco.gobbi on 07/01/14.
 */
requirejs.config({
    paths: {
        "jquery": "../libs/jquery/jquery-1.10.2.min",
        "jquery-private": "../libs/jquery/jquery-private",
        'Utils': '../common/Utils',
        "angular": "../libs/angular/1.2.7/angular.min",
//        "angular-animate": "../libs/angular/1.2.7/angular-animate.min",
//        "angular-resource": "../libs/angular/1.2.7/angular-resource.min",
//        "angular-route": "../libs/angular/1.2.7/angular-route.min",
        "angular-ui-router": "../libs/angular/angular-ui-router.min",
        "TweenMax": "../libs/TweenMax",
        "hammer": "../libs/hammer/hammer",
        "hammer-fakemultitouch": "../libs/hammer/hammer.fakemultitouch",
        "hammer-showtouches": "../libs/hammer/hammer.showtouches",
        "EventDispatcher": "../libs/EventDispatcher",
        "SocketServiceEvent": "../events/SocketServiceEvent",
        "socket.io": "../libs/socket.io",
        "delivery": "../libs/delivery",
        "signals": "../libs/signals.min",
        "fabric": "../libs/fabric.custom"
//        "fabric": "../libs/all.require"

    },
    map: {
        '*': { 'jquery': 'jquery-private' },
        'jquery-private': { 'jquery': 'jquery' }
    },
    shim: {
        "socket.io": {
            exports: 'io'
        },
        "hammer":{
            exports:"Hammer"
        },
        "hammer-fakemultitouch":{
            deps: ["hammer"]
        },
        "hammer-showtouches":{
            deps: ["hammer"]
        },
        TweenMax:{
          exports:"TweenMax"
        },
//        "angular-route": {
//            deps: ["angular"]
//        },
//        "angular-resource": {
//            deps: ["angular"]
//        },
//        "angular-animate": {
//            deps: ["angular"]
//        },
        "angular-ui-router": {
            deps: ["angular"]
        }
    }

});
require(["Application"], function (App) {
    App.initialize();
})