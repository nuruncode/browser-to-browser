/**
 * Created by marco.gobbi on 10/01/14.
 */
define(["fabric"], function (_fabric) {

    var canvas,
        image,
        text;


    function ImageTransform() {

    }

    ImageTransform.prototype = {
        init: function (elementID) {
            canvas = new fabric.Canvas(elementID);
            canvas.selection = true;
            canvas.allowTouchScrolling = false;


        },
        disableImage: function () {
            if (!image) return;
            image.set('selectable', false);
            image.hasControls=true;
            image.hasBorders=true;
            canvas.renderAll();
        },
        enableImage: function () {
            if (!image)   return;
            image.set('selectable', true);
            image.hasControls=false;
            image.hasBorders=false;
            canvas.renderAll();
        },
        setImage: function (imgPath) {
            fabric.Image.fromURL(imgPath, function (oImg) {
                if (image) canvas.remove(image);
                image = oImg;

                image.lockUniScaling = true;
                /**/ image.centeredRotation = true;
                image.centeredScaling = true;
                /*image.hasControls = false;
                 image.hasBorders = false;*/
                image.minScaleLimit = 0.5;
                canvas.add(oImg);
                image.set('selectable', true);
                image.sendToBack();
            });

        },
        disableText: function () {
            if (!text)return;
            text.set('selectable', false);
            text.hasControls=false;
            text.hasBorders=false;
            canvas.renderAll();
        },
        enableText: function () {
            if (!text)return ;
            text.set('selectable', true);
            text.hasControls=false;
            text.hasBorders=false;
            canvas.renderAll();
        },
        setText: function (value) {
            if (!text) {
                text = new fabric.Text(value, {
                    fontSize: 40,
                    shadow: 'rgba(0,0,0,0.4) 0 0 2px'
                });
                text.lockUniScaling = true;
                 text.centeredRotation = true;
                text.centeredScaling = true;
                canvas.add(text);
            } else {
                text.setText(value);
                canvas.renderAll();
            }
        },
        setTextFont: function (value) {
            if (!text)return;
            text.setFontFamily(value);
            canvas.renderAll();
        },
        setTextColor: function (value) {
            if (!text)return;
            text.setColor(value);
            canvas.renderAll();
        }
    }
    return ImageTransform;
})