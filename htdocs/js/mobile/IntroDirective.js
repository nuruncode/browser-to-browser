/**
 * Created by marco.gobbi on 07/01/14.
 */
define(["jquery", "SocketServiceEvent"], function ($, SocketServiceEvent) {
    return function ($state, $stateParams, applicationModel, $rootScope) {
        return{
            restrict: "EA",
            link: function ($scope, element, attrs) {

                function enableFileUpload() {

                    $(element).find("#upload-file-container").on("change", function (e) {

                        var file = e.target.files[0];
                        applicationModel.sendFile(file).then(function (data) {
                            $rootScope.cImage = data.path;
                            console.log("FILE INVIATO", $rootScope.cImage);
                            $state.go("index.step1");
                        });
                    });

                };
                $scope.$on("$viewContentLoaded", function (event) {
                    switch ($state.current.name) {
                        case "intro":
                            enableFileUpload();
                            break;
                    }
                });
                $scope.$on("$destroy", function (e) {
                    console.log("DESTROY LOGIN!!!!");
                    applicationModel.socketService.removeEventListener(SocketServiceEvent.READY, _handleReady);
                })

                function _handleReady(e) {
                    console.log("MOBILE READY");
                    applicationModel.isLogged=true;
                    applicationModel.socketService.emit(SocketServiceEvent.MOBILE_CONNECTED);
                }

                applicationModel.socketService.addEventListener(SocketServiceEvent.READY, _handleReady);
                //var token=atob($stateParams.token);
                var params=JSON.parse(atob($stateParams.token));
                params.token=$stateParams.token;
                console.log(params);
                applicationModel.socketService.joinUser(params).then(function () {
                    console.log("USER JOINED");
                })

            }
        }
    }
});