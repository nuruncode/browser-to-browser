/**
 * Created by marco.gobbi on 07/01/14.
 */
define([
    "jquery",
    "SocketServiceEvent",
    "ImageTransform"
], function ($, SocketServiceEvent,  ImageTransform) {
    return function ($rootScope, $timeout, $state, applicationModel) {
        return{
            restrict: "EA",
            link: function ($scope, element, attrs) {
                var imageTransform = new ImageTransform();
                imageTransform.init("picture");

                function enableFileUpload() {
                    $('#file').on("change", function (e) {
                        console.log("FILE CHANGED");
                        var file = e.target.files[0];
                        applicationModel.sendFile(file).then(function(data){
                            console.log("FILE INVIATO", $scope.cImage);
                            $scope.cImage = data.path;
                            imageTransform.setImage(data.path);
                            $scope.$apply();
                        });
                    });

                };

                $scope.$on("$viewContentLoaded", function (event) {
                    $scope.isPicture = false;
                    $scope.isText = false;
                    $scope.isSwipe = false;
                    //
                    imageTransform.disableImage();
                    imageTransform.disableText();
                    switch ($state.current.name) {
                        case "index.step4":
                            $scope.isText = true;
                            imageTransform.enableText();
                            break;
                        case "index.step1":
                            $scope.isPicture = true;
                            enableFileUpload();
                            imageTransform.enableImage();
                            break;
                        case "index.step5":
                            $scope.isSwipe = true;
                            break;
                        case "index.step2":
                            imageTransform.enableText();
                            break;
                        default :
                            break;
                    }
                });
                $scope.$watch("cText", function (value, _old) {
                    if (!value)return;
                    imageTransform.setText(value);
                })
                $scope.$watch("cColorClass",function(value,old){
                   if(!value)return;
                    imageTransform.setTextColor(value);
                });
                $scope.$on("$destroy", function (e) {
                    console.log("DESTROY INDEX!!!!");
                })
                if($scope.cImage){
                    imageTransform.setImage($scope.cImage);
                }
                if($scope.cColorClass){
                    imageTransform.setTextColor($scope.cColorClass);
                }

            }

        }
    }
});