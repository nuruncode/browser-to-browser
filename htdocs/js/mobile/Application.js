/**
 * Created by marco.gobbi on 07/01/14.
 */
define([
    "jquery",
    "IntroDirective",
    "IndexDirective",
    "SwipeDirective",
    "ApplicationModel",
    "SocketServiceEvent",
    "angular",
    "angular-ui-router"
], function ($, IntroDirective, IndexDirective, SwipeDirective, ApplicationModel,SocketServiceEvent) {

    function initialize() {
        var module = angular.module("mobile", ["ui.router"]);
        module.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
            //
            // For any unmatched url, redirect to /state1
            $urlRouterProvider.otherwise("/login");
            //
            // Now set up the states
            $stateProvider
                .state("login",{
                    url:"/login",
                    templateUrl: "mobile/login.html",
                    controller:["$scope","applicationModel",function($scope,applicationModel){
                        applicationModel.socketService.emit(SocketServiceEvent.MOBILE_DISCONNECTED);
                    }]
                })
                .state('intro', {
                    url: "/intro/{token}",
                    templateUrl: "mobile/intro.html"
                })
                .state('index', {
                    url: "/index",
                    abstract: true,
                    templateUrl: "mobile/index.html",
                    controller: ["$scope", "$rootScope","$state","applicationModel", function ($scope, $rootScope,$state,applicationModel) {
                        if(!applicationModel.isLogged){
                            $state.go("login");
                            return;
                        }
                        $scope.finalImage = "";
                        $scope.isSwipe = false;
                        $scope.isPicture = false;
                        $scope.isText = false;
                        $scope.cFontClass = "font-arial";
                        $scope.cColorClass = "#000000";
                        $scope.cImage = $rootScope.cImage;
                        $scope.cText = "";
                        $scope.getImage = function () {
                            return $scope.cImage;
                        }

                    }]
                })
                .state('index.step1', {
                    url: "/step1",
                    templateUrl: "mobile/index.step1.html"
                })
                .state('index.step2', {
                    url: "/step2",
                    templateUrl: "mobile/index.step2.html",
                    controller: ["$scope", function ($scope) {
                        //console.log("step2", $scope);
                    }]
                })
                .state('index.step3', {
                    url: "/step3",
                    templateUrl: "mobile/index.step3.html",
                    controller: ["$scope", function ($scope) {
                        $scope.setFontClass = function (value) {

                            $scope.$parent.cFontClass = value;
                        }
                    }]
                })
                .state('index.step4', {
                    url: "/step4",
                    templateUrl: "mobile/index.step4.html",
                    controller: ["$scope", function ($scope) {
                        $scope.setTextColor = function (value) {
                            console.log(value);
                            $scope.$parent.cColorClass = value;
                        }
                    }]
                })
                .state('index.step5', {
                    url: "/step5",
                    templateUrl: "mobile/index.step5.html"
                })


        }]);
        module.service("applicationModel", function () {
            var model = new ApplicationModel();

            model.SOCKET_REQUEST = window.Constants.Socket.endpoint;
            model.SAVE_IMAGE_REQUEST = window.Constants.Application.SAVE_IMAGE_REQUEST;
            model.initialize();
            return model;
        });
//
        module.directive("introView", ['$state', '$stateParams', 'applicationModel', '$rootScope', IntroDirective]);
        module.directive("indexView", ['$rootScope', '$timeout', '$state', 'applicationModel', IndexDirective]);
        module.directive("swipeView", ['applicationModel', '$timeout', '$state', SwipeDirective]);

        angular.bootstrap(document, ["mobile"]);

    }

    return {
        initialize: initialize
    }
});