/**
 * Created by marco.gobbi on 13/01/14.
 */
define([
    "jquery",
    "../common/SocketService",
    "../common/LoginService"], function ($, SocketService, LoginService) {
    function dataURLtoBlob(dataURI) {
        // convert base64 to raw binary data held in a string
        // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
        var byteString = atob(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to an ArrayBuffer
        var ab = new ArrayBuffer(byteString.length);
        var ia = new Uint8Array(ab);
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        // write the ArrayBuffer to a blob, and you're done
        var blob = new Blob([ab], {type: mimeString});

        return   blob;

    }

    var socketService,
        loginService,
    // loginDeferred = $.Deferred(),
        imageDeferred = $.Deferred();


    function ApplicationModel() {
        this.SAVE_IMAGE_REQUEST = "";
        this.SOCKET_REQUEST = "";
        this.LOGIN_REQUEST = "";
        this.socketService = socketService = new SocketService();
        // this.loginService = loginService = new LoginService();
        this.isLogged = false;
    };
    ApplicationModel.prototype = {
        initialize: function () {
            socketService.addEventListener(SocketService.Event.FILE_SAVED, function (e) {
                imageDeferred.resolve.apply(imageDeferred, [e.data]);
            });
            socketService.init(this.SOCKET_REQUEST);
            socketService.addEventListener(SocketService.Event.FULL, function () {
                alert("C'è gia qualcuno!!");
            });

        },
        saveFromDataURL: function (dataURL) {
            imageDeferred = $.Deferred();
            var file = dataURLtoBlob(dataURL);
            socketService.sendFile(file);
            return imageDeferred.promise();
        },
        login: function () {
            /*loginService.login();
             return loginDeferred.promise();*/
        },
        sendFile: function (file) {
            var deferred = $.Deferred();
            var formData = new FormData();
            formData.append("fileToUpload", file);
            $.ajax({
                url: this.SAVE_IMAGE_REQUEST,
                data: formData,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    console.log(JSON.parse(data))
                    deferred.resolve.apply(deferred, [JSON.parse(data)]);
                }
            });
            return deferred.promise();
        }
    }
    return ApplicationModel;
})