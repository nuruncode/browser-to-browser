/**
 * Created by marco.gobbi on 09/01/14.
 */
define(["hammer", "TweenMax", "signals"], function (Hammer, TweenMax, signals) {
    var scope,
        GESTURES = 'drag dragend swipe';

    /* Hammer Gestures variables*/
    var posX = 0, posY = 0,
        lastPosX = 0, lastPosY = 0,
        element;
    var TIME;

    function _handleHammer(ev) {
        TIME = .1;
        ev.stopPropagation();
        switch (ev.type) {
            case 'drag':
                posX = 0 //Math.min(ev.gesture.deltaX + lastPosX, 0);
                posY = Math.min(ev.gesture.deltaY + lastPosY, 0);
                break;
            case 'dragend':
                posX = 0;
                posY = 0;
                lastPosX = posX;
                lastPosY = posY;
                TIME = .6;
                break;
            case "swipe":
                scope.onSwipe.dispatch(posY);
                break;
        }
        TweenMax.to(element, TIME, {
            y: posY
        });
    }

    function PosizionamentoImage() {
        this.hammertime;
        this.onSwipe = new signals.Signal();
        scope = this;
    }

    PosizionamentoImage.prototype = {
        init: function (target, _element) {
            element = _element;
            this.hammertime = Hammer(target[0], {
                drag_max_touches: 0,
                drag_block_horizontal: true,
                drag_block_vertical: true,
                drag_min_distance: 0
            });
            this.hammertime.on(GESTURES, _handleHammer);
        },
        enable: function () {
            this.hammertime.enable(true);
        },
        disable: function () {
            this.hammertime.enable(false);
        },
        reset: function () {
        },
        dispose: function () {
            console.log("DISPOSE");
            this.disable();
            this.onSwipe.removeAll();
            this.hammertime.off(GESTURES, _handleHammer);
        }
    }
    return PosizionamentoImage;
})