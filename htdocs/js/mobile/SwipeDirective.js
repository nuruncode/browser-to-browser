/**
 * Created by marco.gobbi on 09/01/14.
 */
define(["jquery", "PosizionamentoImage", "SocketServiceEvent"], function ($, PosizionamentoImage, SocketServiceEvent) {

    return function (applicationModel, $timeout, $state) {
        var STEP_NAME = "index.step5",
            swipe;
        return{
            restrict: "EA",

            link: function ($scope, element, attrs) {
                var timeoutPromise=$timeout(function () {
                    try{
                        var dataURL = document.getElementById("picture").toDataURL();
                        applicationModel.saveFromDataURL(dataURL).then(function (data) {
                            console.log("IMMAGINE COMPOSITA", data.path);
                        })
                    }catch (e){

                    }

                }, 100);

                swipe = new PosizionamentoImage();
                swipe.onSwipe.add(function (posY) {
                    applicationModel.socketService.emit(SocketServiceEvent.MESSAGE_FROM_GAME);
                });
                swipe.init($("#container-swipe"), $("#container-swipe"));
                $scope.$on("$stateChangeStart", function (event) {
                    if ($state.current.name != STEP_NAME) {
                        $timeout.cancel(timeoutPromise);
                        if (swipe) {
                            swipe.dispose();
                        }
                        applicationModel.socketService.removeEventListener(SocketServiceEvent.FILE_SAVED, _handleFileSaved);
                        applicationModel.socketService.removeEventListener(SocketServiceEvent.DELIVERY_CONNECT, _handleDeliveryConnect);
                    } else {

                    }
                });
                applicationModel.isLogged = false;
            }
        }
    }
})