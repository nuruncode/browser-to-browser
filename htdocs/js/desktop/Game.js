/**
 * Created by marco.gobbi on 23/12/13.
 */
define(["jquery", "TweenMax"], function ($, TweenMax) {
    var width,
        height;

    function Game() {
        width = $(window).width();
        height = $(window).height();


        TweenMax.set($("#picture"), {
            x: width / 2 - 24,
            y: height / 2 - 24
        });

        $("#container").height(height);
    }

    Game.prototype = {
        muoviImmagine: function (data) {
            var a = (160 - data.linearVelocity.x);
            var b = 280;
            var _x2 = (a / b) * (height / 2 + b);
            var x2 = (width / 2) - _x2;

            TweenMax.set($("#picture"), {x: width/2, y: height});
            TweenMax.to($("#picture"), 1, {
                x: x2 - 24,
                y: height / 2 - 24,
                ease: Sine.easeOut
            })
        }
    }
    return Game
});