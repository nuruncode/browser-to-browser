/**
 * Created by marco.gobbi on 13/01/14.
 */
define(["jquery", "SocketServiceEvent"], function ($, SocketServiceEvent) {
    return function (applicationModel, $state) {
        return {
            restrict: "EA",
            link: function ($scope, element, attrs) {
                function _handleFileSaved(event) {
                    $scope.cImage = event.data.path;
                    $scope.$apply();
                }

                function _handleMessageFromGame() {
                    TweenMax.fromTo($("#picture"), 1, {
                        x: 0,
                        y: 500,
                        scale: 2,
                        alpha: 0,
                        rotation: Math.random() * 180 - 90
                    }, {
                        x: 0,
                        y: 0,
                        alpha: 1,
                        scale: 1,
                        ease: Expo.easeOut,
                        rotation: 0
                    });

                }

                function _handleDisconnect(e) {
                    applicationModel.isLogged = false;
                    $state.go("login");
                }

                applicationModel.socket.addEventListener(SocketServiceEvent.FILE_SAVED_BROADCAST, _handleFileSaved);
                applicationModel.socket.on(SocketServiceEvent.MESSAGE_FROM_GAME, _handleMessageFromGame);
                applicationModel.socket.addEventListener(SocketServiceEvent.MOBILE_DISCONNECTED, _handleDisconnect);
                $scope.$on("$destroy", function () {
                    applicationModel.socket.removeEventListener(SocketServiceEvent.FILE_SAVED_BROADCAST, _handleFileSaved);
                    applicationModel.socket.removeEventListener(SocketServiceEvent.MOBILE_DISCONNECTED, _handleDisconnect);
                })
                if (!applicationModel.isLogged) {
                    $state.go("login");
                }
            }
        }
    }
})