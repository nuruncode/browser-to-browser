/**
 * Created by marco.gobbi on 20/12/13.
 */
define([
    "jquery",
    "Model",
    "SocketServiceEvent",
    "TweenMax",
    "LoginDirective",
    "IndexDirective",
    //
    "angular",
    "angular-ui-router"
], function ($, Model, SocketServiceEvent, TweenMax,LoginDirective,IndexDirective) {



    function _handleMessageFromGame() {
        TweenMax.fromTo($("#picture"), 1, {
            x: 0,
            y: 500,
            scale: 2,
            alpha: 0,
            rotation: Math.random() * 180 - 90
        }, {
            x: 0,
            y: 0,
            alpha: 1,
            scale: 1,
            ease: Expo.easeOut,
            rotation: 0
        });

    }

    function initialize() {

        var module = angular.module("mobile", ["ui.router"]);
        module.config(['$stateProvider', '$urlRouterProvider',function ($stateProvider, $urlRouterProvider) {
            //
            // For any unmatched url, redirect to /state1
            $urlRouterProvider.otherwise("/login");
            //
            // Now set up the states
            $stateProvider
                .state('login', {
                    url: "/login",
                    templateUrl: "desktop-partials/login.html"
                })
                .state('index', {
                    url: "/index",
                    templateUrl: "desktop-partials/index.html"
                })

        }]);
        module.service("applicationModel", function () {


            var model = new Model();
            model.facebook.init(window.Constants.FB);
            model.applicationService.init(window.Constants.Application);
            model.socket.init(window.Constants.Socket.endpoint);
            return model;
        });

        module.directive("loginView", ['applicationModel','$state',LoginDirective]);
        module.directive("indexView", ['applicationModel',"$state",IndexDirective]);

        angular.bootstrap(document, ["mobile"]);
    }
    return {
        initialize: initialize
    }
})
;