/**
 * Created by marco.gobbi on 23/12/13.
 */
define(["jquery"], function ($) {
    var settings;

    function ApplicationService() {
    }

    ApplicationService.prototype = {
        init: function (_settings) {
            settings = _settings
        },
        getUser: function (user) {
            var deferred = $.Deferred();
            $.post(settings.user_request,user,
                function (response, status) {
                    response=JSON.parse(response);
                    if (response.status) {
                        deferred.resolve.apply(deferred, [response.data]);
                    } else {
                        deferred.reject.apply(deferred, arguments);
                    }

                }
            );
            return deferred.promise();
        }
    }
    return new ApplicationService()
})