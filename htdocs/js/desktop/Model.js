/**
 * Created by marco.gobbi on 23/12/13.
 */
define(["jquery",
    "EventDispatcher",
    "../common/SocketService",
    "../common/FacebookService",
    "ApplicationService",
    "qrcode"
], function ($, EventDispatcher, SocketService, FacebookService, _appService, qrcode) {
    var fb,
        socketService,
        appService,
        scope;

    function Model() {
        this.facebook = fb = new FacebookService();
        this.socket = socketService = new SocketService();
        this.applicationService = appService = _appService;
        this.isLogged = false;
        scope = this;
    }


    // Model.prototype;
    Model.prototype = Object.create(EventDispatcher.prototype);
    // Remember the constructor property was set wrong, let's fix it
    Model.prototype.constructor = Model;
    Model.prototype = $.extend(Model.prototype, {

        getQRCode: function (htmlElement, params) {
            scope.isLogged=false;
            var _htmlElement = htmlElement;
            var _params = params || {};
            var deferred = $.Deferred();
            fb.me().then(function (fb_user) {
                appService.getUser(fb_user).then(function (app_user) {
                    var _appUser = app_user;
                    socketService.joinUser(app_user).then(function () {
                        scope.isLogged=true;

                        _params.text = _params.text + "/" + _appUser.token;
                        var qrcode = new QRCode(_htmlElement, _params);
                        deferred.resolve.apply(deferred, [qrcode, _params.text]);
                    })
                });
            });
            return deferred.promise();

        }

    });
    return  Model;
});