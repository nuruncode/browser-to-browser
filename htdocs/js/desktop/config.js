/**
 * Created by marco.gobbi on 20/12/13.
 */
require.config({
    waitSeconds: 10,
    paths: {
        "jquery": "../libs/jquery/jquery-1.10.2.min",
        "jquery-private": "../libs/jquery/jquery-private",
        "EventDispatcher": "../libs/EventDispatcher",
        "SocketServiceEvent": "../events/SocketServiceEvent",
        "socket.io": "../libs/socket.io",
        "delivery": "../libs/delivery",
        "signals": "../libs/signals.min",
        "TweenMax": "../libs/TweenMax",
        'facebook': '//connect.facebook.net/en_US/all',
        'qrcode': '../libs/qrcode.min',
        "angular": "../libs/angular/1.2.7/angular.min",
//        "angular-animate": "../libs/angular/1.2.7/angular-animate.min",
//        "angular-resource": "../libs/angular/1.2.7/angular-resource.min",
//        "angular-route": "../libs/angular/1.2.7/angular-route.min",
        "angular-ui-router": "../libs/angular/angular-ui-router.min"

    },
    map: {
        '*': { 'jquery': 'jquery-private' },
        'jquery-private': { 'jquery': 'jquery' }
    },
    shim: {
        TweenMax:{
            exports:"TweenMax"
        },
        "socket.io": {
            exports: 'io'
        },
//        "angular-route": {
//            deps: ["angular"]
//        },
//        "angular-resource": {
//            deps: ["angular"]
//        },
//        "angular-animate": {
//            deps: ["angular"]
//        },
        "angular-ui-router": {
            deps: ["angular"]
        }
    }

});
require(["Application"], function (Application) {
    Application.initialize();
})