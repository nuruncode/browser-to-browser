/**
 * Created by marco.gobbi on 13/01/14.
 */
define(["SocketServiceEvent"], function (SocketServiceEvent) {
    return function (applicationModel,$state) {
        return {
            restrict: "EA",

            link: function ($scope, element, attrs) {
                //console.log("WEEE",attrs.qrCode,attrs);
                $scope.getQRCode = function () {
                    applicationModel.getQRCode(document.getElementById("qrcode"), {
                        text: attrs.qrCode
                    })
                        .then(function (qrcode,path) {
                            console.log(path);
                        })
                }
                applicationModel.socket.on(SocketServiceEvent.MOBILE_CONNECTED,function(){
                    $state.go("index");
                });
            }
        }
    }
})