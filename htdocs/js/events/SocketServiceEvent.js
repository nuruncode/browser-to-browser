/**
 * Created by marco.gobbi on 20/12/13.
 */
!function () {
    var SocketServiceEvent = function () {
        return {
            IMAGE_SENT: "image sent",
            CONNECT: "connect",
            CREATE_OR_JOIN: "create or join",
            READY: "ready",
            FULL: "full",
            LOG: "log",
            MESSAGE: "message",
            MESSAGE_FROM_GAME: "message from game",
            DELIVERY_CONNECT: "delivery.connect",
            SEND_SUCCESS: "send.success",
            FILE_SAVED: "file saved",
            FILE_SAVED_BROADCAST: "file-saved-broadcast",
            ERROR: "error",
            MOBILE_CONNECTED:"mobile connected",
            MOBILE_DISCONNECTED:"mobile disconnected",
            NO_USER_SONNECTED: {
                type: "non user connected",
                message: "non c'è nessuno"
            }
        }
    };
    if (typeof define === "function") {
        define([], SocketServiceEvent);
    } else if (exports) {
        module.exports = SocketServiceEvent();
    }

}();

