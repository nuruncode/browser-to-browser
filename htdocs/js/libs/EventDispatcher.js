/**
 * Created by marco.gobbi on 20/12/13.
 */
define("EventDispatcher", [], function () {
    function EventDispatcher(target) {
        this._listeners = {};
        this._eventTarget = target || this;
    }

    EventDispatcher.prototype = {
        addEventListener: function (type, callback, scope, useParam, priority) {
            priority = priority || 0;
            var list = this._listeners[type],
                index = 0,
                listener, i;
            if (list == null) {
                this._listeners[type] = list = [];
            }
            i = list.length;
            while (--i > -1) {
                listener = list[i];
                if (listener.c === callback && listener.s === scope) {
                    list.splice(i, 1);
                } else if (index === 0 && listener.pr < priority) {
                    index = i + 1;
                }
            }
            list.splice(index, 0, {c: callback, s: scope, up: useParam, pr: priority});
            /*if (this === _ticker && !_tickerActive) {
                _ticker.wake();
            }*/
        },

        removeEventListener: function (type, callback) {
            var list = this._listeners[type], i;
            if (list) {
                i = list.length;
                while (--i > -1) {
                    if (list[i].c === callback) {
                        list.splice(i, 1);
                        return;
                    }
                }
            }
        },

        dispatchEvent: function (type,data) {
            var list = this._listeners[type],
                i, t, listener;
            if (list) {
                i = list.length;
                t = this._eventTarget;
                while (--i > -1) {
                    listener = list[i];
                    if (listener.up) {
                        listener.c.call(listener.s || t, {type: type, target: t,data:data});
                    } else {
                        listener.c.call(listener.s || t,{type: type, target: t,data:data});
                    }
                }
            }
        }
    }

    return EventDispatcher;
});