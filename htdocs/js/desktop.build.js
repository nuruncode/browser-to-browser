/**
 * Created by marco.gobbi on 14/01/14.
 */

({
    baseUrl: "./desktop/",
    mainConfigFile: "./desktop/config.js",
    name: "config",
    out: "./dist/desktop.min.js",
    preserveLicenseComments: false,
    paths: {
        requireLib: "../libs/require",
        facebook:"empty:"
    },
    wrap: false,
    optimize: 'uglify2',
    generateSourceMaps: false,
    include: ["requireLib"]
})