/**
 * Created by marco.gobbi on 07/01/14.
 */
define([
    "jquery",
    "EventDispatcher",
    "../events/LoginServiceEvent"], function ($, EventDispatcher, LoginServiceEvent) {
    var socket,
        scope;

    function LoginService() {
        EventDispatcher.call(this);
        scope = this;
    }

    LoginService.prototype = Object.create(EventDispatcher.prototype);
    // Remember the constructor property was set wrong, let's fix it
    LoginService.prototype.constructor = LoginService;

    LoginService.prototype = $.extend(LoginService.prototype, {
        init: function (_url) {
            socket = io.connect(_url);
            socket.on(LoginServiceEvent.LOGIN, function () {
                scope.dispatchEvent(LoginServiceEvent.LOGIN,arguments);
            });
        }
    })
    LoginService.Event = LoginServiceEvent;
    return LoginService
});