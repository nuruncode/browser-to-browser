/**
 * Created by marco.gobbi on 20/12/13.
 */
define([
    "jquery",
    "EventDispatcher",
    "SocketServiceEvent",
    "socket.io",
    "delivery"
], function ($, EventDispatcher, SocketServiceEvent) {
    var socket;
    var user = {
            name: "marco",
            fb_id: "123456",
            id: "7890"
        },
        scope,
        delivery,
        deferred = {
            ready: {}
        };


    //'http://webrtc.local:2503/image'
    function SocketService(_url) {
        EventDispatcher.call(this);
        scope = this;
    }

    // SocketService.prototype;
    SocketService.prototype = Object.create(EventDispatcher.prototype);
    // Remember the constructor property was set wrong, let's fix it
    SocketService.prototype.constructor = SocketService;
    SocketService.prototype = $.extend(SocketService.prototype, {
        init: function (_url) {
            socket = io.connect(_url);

            socket.on(SocketServiceEvent.READY, function () {
                scope.dispatchEvent(SocketServiceEvent.READY);
                deferred.ready.resolve.apply(deferred.ready, arguments);
                console.log("I'm ready");
            })
            socket.on(SocketServiceEvent.FULL, function (user) {
                scope.dispatchEvent(SocketServiceEvent.FULL, user);
                console.log("C'è già qualcuno", user);
            })
            socket.on(SocketServiceEvent.LOG, function (array) {
                console.log.apply(console, array);
            });
            socket.on(SocketServiceEvent.MESSAGE, function (message) {
                scope.dispatchEvent(message.type, message);
            })

            socket.on(SocketServiceEvent.CONNECT, function () {
                delivery = new Delivery(socket);
                delivery.on(SocketServiceEvent.DELIVERY_CONNECT, function (delivery) {
                    scope.dispatchEvent(SocketServiceEvent.DELIVERY_CONNECT, delivery);

                });
                delivery.on(SocketServiceEvent.SEND_SUCCESS, function (fileUID) {
                    scope.dispatchEvent(SocketServiceEvent.SEND_SUCCESS);
                    console.log("file was successfully sent.");
                });
            });

            socket.on(SocketServiceEvent.FILE_SAVED, function (image) {
                console.log("FILE SAVED", arguments);
                scope.dispatchEvent(SocketServiceEvent.FILE_SAVED, image);
            });
            socket.on(SocketServiceEvent.FILE_SAVED_BROADCAST, function (image) {
                console.log(SocketServiceEvent.FILE_SAVED_BROADCAST, arguments);
                scope.dispatchEvent(SocketServiceEvent.FILE_SAVED_BROADCAST, image);
            });
            socket.on(SocketServiceEvent.ERROR,function(message){
                console.log("SocketServiceEvent.ERROR", arguments);
                scope.dispatchEvent(SocketServiceEvent.ERROR, message);
            });
            socket.on(SocketServiceEvent.MOBILE_DISCONNECTED,function(e){
                console.log("SocketServiceEvent.MOBILE_DISCONNECTED", arguments);
                scope.dispatchEvent(SocketServiceEvent.MOBILE_DISCONNECTED, e);
            });
        },
        joinUser: function (_user) {
            user = _user;
            deferred.ready = $.Deferred();
            socket.emit('create or join', _user);
            return deferred.ready.promise();
        },
        emit: function () {
            socket.emit.apply(socket, arguments);
        },
        on: function () {
            socket.on.apply(socket, arguments);
        },
        sendFile: function (file) {
            delivery.send(file);
        }
    });
    SocketService.Event = SocketServiceEvent

    return SocketService;
});