/**
 * Created by marco.gobbi on 11/11/13.
 */
define(["jquery", 'facebook'], function ($) {
    var user,
        logged = false,
        settings;

    function FacebookService() {}

    FacebookService.prototype = {
        init: function (_settings) {
            if (!_settings)throw new Error("FacebookService deve settato!");
            settings = _settings;
            settings.fields = settings.fields || ["id", "name", "picture"];
            FB.init(_settings);
            // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
            // for any authentication related change, such as login, logout or session refresh. This means that
            // whenever someone who was previously logged out tries to log in again, the correct case below
            // will be handled.
            FB.Event.subscribe('auth.authResponseChange', function (response) {
                // Here we specify what we do with the response anytime this event occurs.
                if (response.status === 'connected') {
                    // The response object is returned with a status field that lets the app know the current
                    // login status of the person. In this case, we're handling the situation where they
                    // have logged in to the app.
                    logged = true;

                    //  testAPI();
                } else if (response.status === 'not_authorized') {
                    // In this case, the person is logged into Facebook, but not into the app, so we call
                    // FB.login() to prompt them to do so.
                    // In real-life usage, you wouldn't want to immediately prompt someone to login
                    // like this, for two reasons:
                    // (1) JavaScript created popup windows are blocked by most browsers unless they
                    // result from direct interaction from people using the app (such as a mouse click)
                    // (2) it is a bad experience to be continually prompted to login upon page load.
                    logged = false;

                    // FB.login();
                } else {
                    // In this case, the person is not logged into Facebook, so we call the login()
                    // function to prompt them to do so. Note that at this stage there is no indication
                    // of whether they are logged into the app. If they aren't then they'll see the Login
                    // dialog right after they log in to Facebook.
                    // The same caveats as above apply to the FB.login() call here.
                    logged = false;

                    // FB.login();
                }
            });
            FB.getLoginStatus(function (response) {
                logged = response.status === "connected";
                if (logged) {
                    /*FB.api('/me', function (response) {
                     console.log('Good to see you, ' + response.name + '.');
                     });*/
                } else {
                    // FB.login()
                }
            });

        },
        login: function () {
//            console.log("LOGGED IN");
            /* FB.getLoginStatus(function (response) {
             logged = response.status === "connected";
             console.log(response, logged);
             if (logged) {
             FB.api('/me', function (response) {
             console.log('Good to see you, ' + response.name + '.');
             });
             } else {
             FB.login()
             }
             });*/
        },
        me: function () {
            var deferred = $.Deferred();
            var request='/me?fields=' + settings.fields.toString();
            if (logged) {
                FB.api(request, function (response) {
                    if(!response.error){
                        deferred.resolve.apply(deferred, arguments);
                    }else{
                        deferred.reject.apply(deferred, arguments);
                    }

                });
            } else {
                FB.login(function () {
                    FB.api(request, function (response) {
                        if(!response.error){
                            deferred.resolve.apply(deferred, arguments);
                        }else{
                            deferred.reject.apply(deferred, arguments);
                        }
                    });
                });
            }
            return deferred;
        },
        share: function (params) {
            var deferred = $.Deferred();
            FB.ui({
                method: 'feed',
                link: params.link,
                picture: params.picture,
                name: params.name,
                caption: params.caption,
                description: params.description
            }, function (response) {
                if (response) {
                    deferred.resolve.apply(deferred, arguments);
                } else {
                    deferred.reject.apply(deferred, arguments);
                }
            });
            return deferred.promise();
        }

    }
    return FacebookService;
});