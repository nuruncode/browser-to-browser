<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <title>Desktop</title>
    <style type="text/css">
        body {
            margin: 0;
            overflow: hidden;
        }

        #container {
            width: 100%;
            height: 100%;
            background-color: #ffcc00;

        }

        #picture {
            position: absolute;
        }

        #qrcode {
            width: 256px;
            height: 256px;
            position: fixed;
            z-index: 1;
            left: 50%;
        }

        #qrcode img, #qrcode canvas {
            margin-left: -50%
        }
    </style>
    <script>
        <?php
         require_once '../include/env.php';
        ?>
        window.Constants = {
            FB: {
                appId: <?php print(FACEBOOK_APP_ID)?>,
                status: true, // check login status
                cookie: true, // enable cookies to allow the server to access the session
                fields: ["id", "name", "picture"] //fields for '/me' request
            },
            Application: {
                user_request: "<?php print(APPLICATION_USER_REQUEST)?>"

            },
            Socket: {
                endpoint: "<?php print(SOCKET_ENDPOINT) ?>"
            }
        }
    </script>
</head>
<body>
<script type="text/ng-template" id="desktop-partials/login.html">
    <div>
        <login-view qr-code="<?php print(APPLICATION_MOBILE_PAGE)?>">
            <h2>BENVENUTO</h2>
            <a href="javascript:void(0)" ng-click="getQRCode()">LOGGATI E OTTIENI IL QRCODE</a>

            <div id="qrcode"></div>
        </login-view>
    </div>
</script>
<script type="text/ng-template" id="desktop-partials/index.html">
    <div>
        <index-view>
            <img id="picture" ng-src="{{cImage}}">
        </index-view>

    </div>
</script>
<div>
    <h2>#LEVATIILFASTIDIO</h2>

    <div ui-view></div>
</div>


<div id="container">
    <img src="" id="picture">
</div>

<!--DEVELOPMENT-->
<script src="js/libs/require.js" data-main="js/desktop/config.js"></script>
<!--PRODUCTION-->
<!--<script src="js/dist/desktop.min.js"></script>-->
</body>
</html>
