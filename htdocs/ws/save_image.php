<?php
$fileName = $_FILES['fileToUpload']['name'];
$fileType = $_FILES['fileToUpload']['type'];
$fileContent = ($_FILES['fileToUpload']['tmp_name']);

$target_path = $_SERVER['DOCUMENT_ROOT'] . "/img/" . basename($fileName);

$json = json_encode(array(
    'status'=>true,
    'name' => $fileName,
    'type' => $fileType,
    "path"=>"/img/" . basename($fileName)
));
//echo json_encode($_SERVER);

if(move_uploaded_file($fileContent,$target_path)){
    echo $json;
}else{
    echo json_encode(array(
        'status'=>false,
        'name' => $fileName,
        'type' => $fileType,
        "path"=>"/img/" . basename($fileName),
        'target_path'=>$target_path
    ));
};

?>