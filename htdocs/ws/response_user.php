<?php
$fb_id = $_POST["id"];
$id = $fb_id;
$name = $_POST["name"];
$json=json_encode(array(
    "id" => $id,
    "fb_id" => $fb_id,
    "name" => $name
));
$token = base64_encode($json);

echo json_encode(array(
    "status" => true,

    "data" => array(
        "id" => $id,
        "fb_id" => $fb_id,
        "name" => $name,
        "token"=>$token
    )
));
/*{
    "status": true,
    "data": {
        "id": "123",
        "fb_id": "456",
        "name": "Marco",
        "token":"abba"
    }

}*/