<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <title>Mobile</title>
    <!--<base href=".">-->
    <link href="css/mobile.css" rel="stylesheet" type="text/css">
    <link href="css/icons.css" rel="stylesheet" type="text/css">
    <script>
        <?php
        require_once '../include/env.php';
       ?>
        window.Constants = {
            Application: {
                SAVE_IMAGE_REQUEST: "<?php print(SAVE_IMAGE_REQUEST)?>"
            },
            Socket: {
                endpoint: "<?php print(SOCKET_ENDPOINT) ?>"
            }
        }
    </script>
</head>
<body>
<script type="text/ng-template" id="mobile/login.html">
    <div>
        COLLEGATI CON IL QRCODE DEL DESKTOP
    </div>
</script>
<script type="text/ng-template" id="mobile/intro.html">
    <div>
        <intro-view>
            <div id="upload-file-container">
                <input type="file" name="photo"/>
            </div>
        </intro-view>
        <p>SCOTTEX</p>
    </div>
</script>
<script type="text/ng-template" id="mobile/index.html">
    <div>
        <index-view>
            <div id="container-swipe">
                <div id="containerBox" ng-class="{active:isPicture}">
                    <canvas id="picture" width="320" height="320"></canvas>
                </div>
                <div ui-view class="ui-view-container"></div>
            </div>
            <ul class="menu-footer" ng-hide="isSwipe">
                <li class="horizontal-5"><a ui-sref=".step1"><span class="sprite picture_active"></span> </a></li>
                <li class="horizontal-5"><a ui-sref=".step2"><span class="sprite testo_idle"></span></a></li>
                <li class="horizontal-5"><a ui-sref=".step3"><span class="sprite text_format_idle"></span></a></li>
                <li class="horizontal-5"><a ui-sref=".step4"><span class="sprite transform_idle"></span></a></li>
                <li class="horizontal-5"><a ui-sref=".step5"><span class="sprite confirm_idle"></span></a></li>
            </ul>
        </index-view>
    </div>
</script>
<script type="text/ng-template" id="mobile/index.step1.html">
    <div>
        <div id="upload-file-container">
            <input type="file" name="photo" id="file"/>
        </div>
    </div>
</script>
<script type="text/ng-template" id="mobile/index.step2.html">
    <div>
        <textarea placeholder="scrivi il tuo testo" ng-model="$parent.cText" name="cText" class="testo"></textarea>
    </div>
</script>
<script type="text/ng-template" id="mobile/index.step3.html">
    <div>
        <ul class="lista">
            <li class="horizontal-2"><a ng-click="setFontClass('font-arial')" href="javascript:void(0)">Arial</a></li>
            <li class="horizontal-2"><a ng-click="setFontClass('font-trebuchet')"
                                        href="javascript:void(0)">trebuchet</a></li>
            <li class="horizontal-2"><a ng-click="setFontClass('font-times')" href="javascript:void(0)">times</a></li>
            <li class="horizontal-2"><a ng-click="setFontClass('font-arial')" href="javascript:void(0)">Arial</a></li>
            <li class="horizontal-2"><a ng-click="setFontClass('font-trebuchet')"
                                        href="javascript:void(0)">trebuchet</a></li>
            <li class="horizontal-2"><a ng-click="setFontClass('font-times')" href="javascript:void(0)">times</a></li>
        </ul>
    </div>
</script>
<script type="text/ng-template" id="mobile/index.step4.html">
    <div>

        <ul class="lista">
            <li class="horizontal-6"><a ng-click="setTextColor('#000000')" href="javascript:void(0)">
                <div class="circleBase circle-black"></div>
            </a></li>
            <li class="horizontal-6"><a ng-click="setTextColor('#ffcc00')" href="javascript:void(0)">
                <div class="circleBase circle-yellow"></div>
            </a></li>
            <li class="horizontal-6"><a ng-click="setTextColor('#ff0000')" href="javascript:void(0)">
                <div class="circleBase circle-blue"></div>
            </a></li>
            <li class="horizontal-6"><a ng-click="setTextColor('#008000')" href="javascript:void(0)">
                <div class="circleBase circle-green"></div>
            </a></li>
            <li class="horizontal-6"><a ng-click="setTextColor('#FF0000')" href="javascript:void(0)">
                <div class="circleBase circle-red"></div>
            </a></li>
            <li class="horizontal-6"><a ng-click="setTextColor('#ffffff')" href="javascript:void(0)">
                <div class="circleBase circle-white"></div>
            </a></li>
        </ul>
    </div>
</script>
<script type="text/ng-template" id="mobile/index.step5.html">
    <div>
        <swipe-view>
            <div>SWIPE</div>
        </swipe-view>

    </div>
</script>
<div>
    <h2>#LEVATIILFASTIDIO</h2>

    <div ui-view></div>
</div>

<!--DEVELOPMENT-->
<script src="js/libs/require.js" data-main="js/mobile/config.js"></script>
<!--PRODUCTION-->
<!--<script src="js/dist/mobile.min.js"></script>-->
</body>
</html>